import mongoose from 'mongoose';
import 'dotenv/config';

const connectToDB = async()=>{
    const URL = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@newlife.oxr0nw8.mongodb.net/mearnproject`;
    try {
        const conn = await mongoose.connect(URL,{useNewUrlParser:true});
        console.log('Database connected successfully');
    } catch (error) {
        console.error(error.message);
        process.exit(1);
    }
}

export default connectToDB;