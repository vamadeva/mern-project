import express from 'express'
import Notes from '../models/Notes.js'
const router = express.Router();
import fetchUser from '../middleware/fetchUser.js';

router.get('/',fetchUser,async(req,res)=>{
    try {
        const notesData = await Notes.find({user:req.userId});
        return res.status(200).json({'notes':notesData});
    } catch (error) {
      return res.status(500).json({error});  
    }
});

router.post('/add',fetchUser,async(req,res)=>{
    try {
        const { title, description, tag } = req.body

        //* validation 
        if (!title || !description || !tag) {
            return res.status(400).json({ error: "All fields are required" });
        }
        const newNotes = new Notes({
            user:req.userId,
            title,
            description
        });
        const notedata = await newNotes.save();
        return res.status(200).json({notedata,'success':'Notes added successfully.'});
    } catch (error) {
        return res.status(500).json({error});
    }
})

router.put('/updatenote/:id',fetchUser,async(req,res)=>{
    const {title, description, tag} = req.body;
    const {id} = req.params;
    try {
        const note = await Notes.findById({_id : id})
        if (!note) {
            return res.status(404).send("Not Found")
        }
        if (note.user.toString() !== req.userId) {
            return res.status(401).send("Not Allowed");
        }
        const noteData = await Notes.findByIdAndUpdate({_id : id}, {
            $set : {
                title,
                description,
                tag
            }
        }, {new : true})

        res.json({note:noteData, success : "Notes Updated Successfully"})
        
    } catch (error) {
        console.log(error)
        res.status(500).json({error});
    }
})

router.delete('/deletenote/:id',fetchUser,async(req,res)=>{
    
    try {
        let note = await Notes.findById(req.params.id);
        if (!note) { 
            return res.status(404).json({error:"Not Found"});
        }

        //* Allow deletion only if user owns this Note
        if (note.user.toString() !== req.userId) {
            return res.status(401).json({error:"Not Allowed"});
        }

        note = await Notes.findByIdAndDelete(req.params.id);
        return res.json({ "Success": "Note has been deleted", note: note });
    } catch (error) {
        return res.status(500).json({error:"Internal Server Error"});
    }
})

router.get('/getNoteById/:id',fetchUser,async(req,res)=>{
    const {title, description, tag} = req.body;
    const {id} = req.params;
    try {
        const note = await Notes.findById({_id : id})
        if (!note) {
            return res.status(404).send("Not Found")
        }
        if (note.user.toString() !== req.userId) {
            return res.status(401).send("Not Allowed");
        }
        res.json({note, success : "note found."});
         
    } catch (error) {
        console.log(error)
        res.status(500).json({error});
    }
})

export default router;

