import express from 'express';
import User from '../models/User.js';
import bcrypt from 'bcrypt';
import  Jwt  from 'jsonwebtoken';
import fetchUser from '../middleware/fetchUser.js';

const router = express.Router();

router.post('/signup',async (req,res)=>{

    const { firstname,lastname,email,password } = req.body;

    if(!firstname || !lastname || !email || !password){
        return res.status(400).json({ error: "All fields are required" })
    }
    if(!email.includes('@')){
        return res.status(400).json({ error: "Email is not valid" })
    }

    const user = await User.findOne({email:req.body.email});
    if(user){
        return res.status(400).json({error:'user already exit.'})
    }

    try {
        const salt = await bcrypt.genSalt(10);
        const hasedPassword = await bcrypt.hash(req.body.password,salt);
        const newUser = await User({
            firstname,
            lastname,
            email,
            password:hasedPassword
        })
       const savedUser = await newUser.save();
       res.status(200).json({success: "Signup Successfully"})
    } catch (error) {
        return res.status(500).json({error:error});
    }
    
})
router.post('/login',async(req,res)=>{
    const {email,password} = req.body;
    try {
        if(!email || !password){
            return res.status(400).json({error:'All fields are required.'})
        }
        const user = await User.findOne({email});

        if(!user){
            return res.status(400).json({error:'user does not exist'})
        }

        const match = await bcrypt.compare(req.body.password,user.password);

        if(!match){
            return res.status(400).json({error:'password does not match'})
        }

        const token = await Jwt.sign({userId:user._id},process.env.JWT_SECRET,{
            expiresIn:'7d'
        });
        return res.status(200).json({token:token,success:'login successfully'});


    } catch (error) {
        return res.status(500).json({error});
    }
})

router.get('/userDetail',fetchUser,async (req,res)=>{

    try {
        const user = await User.findOne({_id:req.userId});
        if(!user){
            return res.status(400).json({error:'user exit.'})
        }
        res.status(200).json({user,success: "user found"});
    } catch (error) {
        return res.status(500).json({error:error});
    }
    
})
export default router;