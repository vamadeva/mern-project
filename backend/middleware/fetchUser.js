import 'dotenv/config';
import jwt from 'jsonwebtoken';


const fetchUser = (req,res,next)=>{
    const token = req.header('auth-token');
    if(!token){
        return res.status(401).json({error:'please provide token'});
    }
    try {
        var decoded = jwt.verify(token, process.env.JWT_SECRET);
        console.log(decoded.decoded) 
        req.userId = decoded.userId;
        next();
    } catch (error) {
       return res.status(500).json({error}); 
    }
    
}

export default fetchUser;