import mongoose from "mongoose";
const { ObjectId } = mongoose.Schema.Types;

const NotesSchema = mongoose.Schema({
    user:{
        type:ObjectId,
        ref:'user'
    },
    title:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    tag: {
        type: String,
        default: "General"
    },
    date:{
        type:Date,
        default: Date.now
    }
});

const Notes = mongoose.model('notes',NotesSchema);
export default Notes;