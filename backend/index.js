import express from 'express';
import connectToDB from './databases/db.js';
import 'dotenv/config'
import auth from './routes/auth.js';
import note from './routes/notes.js';
import cors from 'cors'
const app = express();

const PORT     = process.env.PORT || 4000;
const BASE_URL = process.env.BASE_URL;
app.use(cors());
app.use(express.json());
app.use('/api/auth',auth);
app.use('/api/notes',note);

app.listen(PORT,()=>console.log(`Server is running at ${BASE_URL}:${PORT}`));

connectToDB();
