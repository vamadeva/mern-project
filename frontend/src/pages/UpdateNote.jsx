import {React,useState,useEffect} from 'react';
import Layout from '../components/Layout';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import axios from 'axios';
import toast from 'react-hot-toast';
import { Navigate, useNavigate,useParams } from 'react-router-dom';

const UpdateNote = () => {

    const navigate = useNavigate();

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [tag, setTag] = useState('');

    //* Get Id From useParams
    const { id } = useParams();

    const getNotesById = ()=>{
        axios.get(process.env.REACT_APP_API_URL + '/api/notes/getNoteById/'+id, {
            headers: {
                'Content-Type': 'application/json',
                'auth-token': localStorage.getItem('token')
            }
        })
        .then(function (response) {
            if (response.data.error) {
                toast.error(response.data.error);
            } else {
                toast.success(response.data.success)
                setTitle(response.data.note.title)
                setTag(response.data.note.tag)
                setDescription(response.data.note.description)
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    useEffect(() => {
        getNotesById();
    }, [id]);

    const updateNote = async ()=>{

        const noteData = {
            title,
            description,
            tag
        }

        axios.put(process.env.REACT_APP_API_URL + '/api/notes/updatenote/'+id,noteData, {
            headers: {
                'Content-Type': 'application/json',
                'auth-token': localStorage.getItem('token')
            }
        })
        .then(function (response) {
            if (response.data.error) {
                toast.error(response.data.error);
            } else {
                toast.success(response.data.success);
                navigate('/notes');
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    return (
        <Layout>
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Typography component="h1" variant="h5">
                    Update Note
                </Typography>
                <Box component="form" noValidate  sx={{ mt: 3 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                autoComplete="given-name"
                                name="title"
                                required
                                fullWidth
                                id="title"
                                label="Title"
                                autoFocus
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                id="description"
                                label="Description"
                                name="description"
                                autoComplete="description"
                                multiline
                                rows={4}
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                name="tag"
                                label="Tag"
                                id="tag"
                                autoComplete="tag"
                                value={tag}
                                onChange={(e) => setTag(e.target.value)}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={updateNote}
                    >
                        Update Note
                    </Button>
                </Box>
            </Box>
        </Layout>
    );
}

export default UpdateNote;