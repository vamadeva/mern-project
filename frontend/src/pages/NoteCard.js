import { React, useEffect, useContext } from 'react';
import Layout from '../components/Layout';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';
import myContext from '../context/data/MyContext';
import Box from '@mui/material/Box';
import {Link} from 'react-router-dom';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));
const NoteCard = () => {

    const context = useContext(myContext)
    const { allNotes, getAllNotes, loading,deleteNote } = context;

    useEffect(() => {
        if (localStorage.getItem('token')) {
            getAllNotes();
        }
    }, []);

    return (
        <Layout>
            <Box sx={{ width: '100%' }}>
                <Stack spacing={2}>
                    {loading ?
                        <>
                            <div className=''>
                                <img className='w-14 py-20' src="https://i.gifer.com/ZZ5H.gif" alt="" />
                            </div>
                        </> :
                        <>
                            {allNotes.length > 0 ? allNotes.map((item) => {
                                const { title, description, tag,_id } = item;

                                return (
                                    <Item>
                                        <Typography variant="h6" gutterBottom sx={{ mt: 3 }}>{title}</Typography>
                                        <span>{description}</span>
                                        <span variant="p" gutterBottom sx={{ mt: 3 }}>{tag}</span>
                                        <div className="del">
                                            <div
                                                onClick={() => { deleteNote(_id) }}>
                                               Delete
                                            </div>
                                        </div>
                                        <Link to={`/notes/edit/${_id}`}>Edit</Link>
                                    </Item>
                                )

                            }) : <Typography variant="h6" gutterBottom sx={{ mt: 3 }}>No notes found</Typography>

                            }
                        </>
                    }

                </Stack>
            </Box>
        </Layout>
    );
}

export default NoteCard;