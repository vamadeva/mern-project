import { React, useContext, useEffect, useState } from 'react';
import Layout from '../components/Layout';
import myContext from '../context/data/MyContext';
import axios from 'axios';
import toast from 'react-hot-toast';
import { Navigate, useNavigate } from 'react-router-dom';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));
const Profile = () => {
    const context = useContext(myContext);
    const { allNotes } = context;
    const [user, setUser] = useState([]);
    const navigate = useNavigate();
    const getUserDetail = () => {
        axios.get(process.env.REACT_APP_API_URL + '/api/auth/userDetail', {
            headers: {
                'Content-Type': 'application/json',
                'auth-token': localStorage.getItem('token')
            }
        })
            .then(function (response) {
                if (response.data.error) {
                    console.log(response.data.error);
                    toast.error(response.data.error);
                } else {
                    toast.success(response.data.success)
                    setUser(response.data.user);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    useEffect(() => {
        getUserDetail();
    }, [])
    return (
        <Layout>
            <Box sx={{ width: '100%' }}>
                <Stack spacing={2}>
                    <Item>
                        <Typography variant="h6" gutterBottom sx={{ mt: 3 }}>{user.firstname} {user.lastname}</Typography>
                        <Typography variant="h6" gutterBottom sx={{ mt: 3 }}>{user.email}</Typography>
                    </Item>
                </Stack>
            </Box>
        </Layout>
    );
}
export default Profile;