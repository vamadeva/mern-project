import {React ,useState} from 'react';
import MyContext from './MyContext';

import axios from 'axios';
import toast from 'react-hot-toast';


const MyState = (props) => {

    const [loading, setLoading] = useState(false);
    const [allNotes, setAllNotes] = useState([]);

    const getAllNotes = async()=>{
        
        axios.get(process.env.REACT_APP_API_URL + '/api/notes', {
            headers: {
                'Content-Type': 'application/json',
                'auth-token': localStorage.getItem('token')
            }
        })
        .then(function (response) {
            if (response.data.error) {
                setLoading(true)
                console.log(response.data.error);
                toast.error(response.data.error);
            } else {
                toast.success(response.data.success)
                setAllNotes(response.data.notes);
                setLoading(false)
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    const deleteNote = (id)=>{
        axios.delete(process.env.REACT_APP_API_URL + '/api/notes/deletenote/'+id, {
            headers: {
                'Content-Type': 'application/json',
                'auth-token': localStorage.getItem('token')
            }
        })
        .then(function (response) {
            if (response.data.error) {
                console.log(response.data.error);
                toast.error(response.data.error);
            } else {
                getAllNotes();
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    return (
        <MyContext.Provider value={{allNotes, getAllNotes,loading,deleteNote}}>
            {props.children}
        </MyContext.Provider>
    );
}


export default MyState;