import React from "react";

import {
    BrowserRouter as Router,
    Route,
    Routes,
    Navigate
} from "react-router-dom";

import Home from './pages/Home';
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import AddNote from "./pages/AddNote";
import UpdateNote from "./pages/UpdateNote";
import NoPage from "./pages/NoPage";
import Profile from "./pages/Profile";
import NoteCard from "./pages/NoteCard";
import MyState from "./context/data/MyState";

const App = () => {
    return (
        <MyState>
            <Router>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/signup" element={<Signup />} />
                    <Route path="/login" element={
                        
                            <Login />
                        
                    } />
                    <Route path="/addnote" element={
                        <ProtectedRoute>
                            <AddNote />
                        </ProtectedRoute>
                    } />
                    <Route path="/notes/edit/:id" element={
                        <ProtectedRoute>
                            <UpdateNote />
                        </ProtectedRoute>
                    } />
                    <Route path="/profile" element={
                        <ProtectedRoute>
                            <Profile />
                        </ProtectedRoute>

                    } />
                    <Route path="/notes" element={
                        <ProtectedRoute>
                            <NoteCard />
                        </ProtectedRoute>

                    } />
                    <Route path="/*" element={<NoPage />} />
                </Routes>
            </Router>
        </MyState>
    )
}

export default App;

export const ProtectedRoute = ({ children }) => {
    const token = localStorage.getItem('token')
    if (token) {
        return children
    }
    else {
        return <Navigate to={'/login'} />
    }
}

export const ProtectedRouteLogin = ({children}) => {
    const token = localStorage.getItem('token')
    if (token) {
        return <Navigate to={'/'} />
    }
    else {
        return children
    }
}